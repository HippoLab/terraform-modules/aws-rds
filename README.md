Terraform AWS RDS Module
========================

Module creates following AWS resources:

- RDS Instance
- RDS Security Group
- RDS Subnet Group
- Number of CloudWatch alarms
- IAM role and policy for RDS enhanced monitoring

# Contents
- [Required Input Variables](#variables)
- [Usage](#usage)
- [Outputs](#outputs)
- [Licence](#licence)
- [Author Information](#author)

## <a name="variables"></a> Required Input Variables
At least following input variables must be provided. See [full list](variables.tf) of supported variables

| Name           | Description                             |
| -------------- | --------------------------------------- |
| name           | Common name - unique identifier         |
| engine         | Database engine, i.e. postgres, mysql   |
| engine_version | Version of the provided Database engine |
| vpc_id         | ID of a VPC resource will be created in |
| subnet_ids     | Subnet IDs service will be attached to  |

## <a name="usage"></a> Usage
Following code provisions RDS MySQL version 8 and puts credentials into AWS secret manager:
```hcl-terraform
module "network" {
  source     = "git::https://gitlab.com/HippoLab/terraform-modules/aws-network.git"
  name       = "${var.project_name} ${var.environment}"
  extra_tags = local.common_tags
}

resource "aws_kms_key" "environment_kms_key" {
  description = "${var.project_name} ${var.environment} KMS key"
  policy      = data.aws_iam_policy_document.environment_kms_key.json
  tags        = local.common_tags
}

resource "aws_kms_alias" "environment_kms_key" {
  name          = "alias/${lower(join("-", [var.project_name, var.environment]))}"
  target_key_id = aws_kms_key.environment_kms_key.key_id
}

module "rds_mysql" {
  source                  = "git::https://gitlab.com/HippoLab/terraform-modules/aws-rds.git"
  name                    = "${var.project_name} ${var.environment}"
  engine                  = "mysql"
  engine_version          = "8.0.21"
  instance_class          = var.environment == "production" ? "db.t3.xlarge" : "db.t3.small"
  parameter_group_name    = aws_db_parameter_group.rds_mysql.name
  vpc_id                  = module.network.vpc_id
  subnet_ids              = module.network.subnet_ids["private"]
  storage_size            = var.environment == "production" ? 2000 : 100
  monitoring_interval     = var.environment == "production" ? 30 : 0
  deletion_protection     = var.environment == "production" ? true : false
  skip_final_snapshot     = var.environment == "production" ? false : true
  kms_key_arn             = aws_kms_key.environment_kms_key.arn
  backup_retention_period = var.environment == "production" ? 7 : 1
  apply_immediately       = true
  extra_tags              = local.common_tags
}

resource "aws_db_parameter_group" "rds_mysql" {
  name   = lower("${var.project_name}-${var.environment}-mysql")
  family = "mysql8.0"

  parameter {
    name  = "log_bin_trust_function_creators"
    value = 1
  }

  tags = local.common_tags
}

resource "aws_secretsmanager_secret" "rds_mysql_master_username" {
  name                    = "/${lower(var.project_name)}/${lower(var.environment)}/rds_mysql_master_username"
  recovery_window_in_days = var.environment == "production" ? 7 : 0
  description             = "${var.project_name} ${var.environment} RDS MySQL master username"
  kms_key_id              = aws_kms_key.environment_kms_key.arn
  tags                    = local.common_tags
}

resource "aws_secretsmanager_secret_version" "rds_mysql_master_username" {
  secret_id     = aws_secretsmanager_secret.rds_mysql_master_username.id
  secret_string = module.rds_mysql.master_username
}

resource "aws_secretsmanager_secret" "rds_mysql_master_password" {
  name                    = "/${lower(var.project_name)}/${lower(var.environment)}/rds_mysql_master_password"
  recovery_window_in_days = var.environment == "production" ? 7 : 0
  description             = "${var.project_name} ${var.environment} RDS MySQL master password"
  kms_key_id              = aws_kms_key.environment_kms_key.arn
  tags                    = local.common_tags
}

resource "aws_secretsmanager_secret_version" "rds_mysql_master_password" {
  secret_id     = aws_secretsmanager_secret.rds_mysql_master_password.id
  secret_string = module.rds_mysql.master_password
}
```

## <a name="outputs"></a> Outputs
Full list of module outputs and their descriptions can be found in [outputs.tf](outputs.tf)

## <a name="licence"></a> Licence
The module is distributed under [MIT Licence](LICENCE.txt). Please make sure you have read, understood and agreed
to its terms and conditions

## <a name="author"></a> Author Information
Vladimir Tiukhtin <vladimir.tiukhtin@hippolab.ru><br/>London
