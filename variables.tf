variable "name" {
  type = string
}

variable "engine" {
  description = "Engine type, example values mysql, postgres"
  type        = string
}

variable "engine_version" {
  description = "Engine version"
  type        = string
}

variable "description" {
  description = "description of the resource"
  type        = string
  default     = ""
}

variable "vpc_id" {
  type = string
}

variable "subnet_ids" {
  description = "Subnet id(s) for ELB"
  type        = list(string)
}

variable "publicly_accessible" {
  description = "Whether or not cluster should be exposed to the internet"
  type        = bool
  default     = false
}

variable "clouwatch_prefix" {
  description = "for those who use more than one database in their stack"
  type        = string
  default     = ""
}

variable "parameter_group_name" {
  type    = string
  default = ""
}

variable "multi_az" {
  description = "Multi-AZ or not"
  type        = bool
  default     = false
}

variable "storage_type" {
  description = "Type of storage: standard , gp2 , or io1  "
  type        = string
  default     = "gp2"
}

variable "storage_size" {
  description = "Storage size in GB"
  type        = number
  default     = 10
}

variable "iops" {
  type    = number
  default = 0
}

variable "storage_encrypted" {
  description = "Whether or not encrypt database storage"
  type        = bool
  default     = true
}

variable "kms_key_arn" {
  description = ""
  type        = string
  default     = null
}

variable "allow_major_version_upgrade" {
  description = "Allow major version upgrade or not"
  type        = bool
  default     = false
}

variable "instance_class" {
  description = "Instance class"
  type        = string
  default     = "db.t3.medium"
}

variable "master_username" {
  description = "User name"
  type        = string
  default     = "root"
}

variable "snapshot_identifier" {
  type    = string
  default = null
}

variable "backup_retention_period" {
  type    = number
  default = 7
}

variable "alarm_cpu_threshold" {
  type    = number
  default = 75
}

variable "alarm_disk_queue_threshold" {
  type    = number
  default = 10
}

variable "alarm_free_disk_threshold" {
  type    = number
  default = 2000000000
}

variable "alarm_actions_list" {
  type    = list(string)
  default = []
}

variable "apply_immediately" {
  description = "Whether or not any changes must be applied immediately or during next maintenance window"
  type        = bool
  default     = false
}

variable "monitoring_interval" {
  description = "Whether or not Advanced monitoring should be turned on. Valid values are 0,1,5,10,15,30,60. Additional charges may apply"
  type        = number
  default     = 0
}

variable "startup_db_name" {
  type    = string
  default = null
}

variable "skip_final_snapshot" {
  description = "Whether or not final snapshot must be created before cluster is deleted"
  type        = bool
  default     = true
}

variable "deletion_protection" {
  type    = bool
  default = true
}

variable "enabled_cloudwatch_logs_exports" {
  description = "Allows the type of log types to enable for exporting to CloudWatch logs"
  type        = list(string)
  default     = []
}

variable "additional_security_group_ids" {
  description = "Any additional Security Group IDs to attach to the service"
  type        = list(string)

  validation {
    condition     = length(var.additional_security_group_ids) <= 4
    error_message = "Only 5 security groups are allowed per network interface. One has been already taken by the module."
  }

  default = []
}

variable "extra_tags" {
  description = "Map of additional tags to add to module's resources"
  type        = map(string)
  default     = {}
}
