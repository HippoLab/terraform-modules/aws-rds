terraform {
  required_version = ">=0.14.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=3.0.0"
    }
  }

}

locals {
  name = lower(substr(join("-", regexall("[[:alnum:]]*", var.name)), 0, 24)) // RFC952 compliant
  common_tags = {
    ManagedBy = "terraform"
  }
}
