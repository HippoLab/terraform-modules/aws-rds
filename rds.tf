resource "aws_db_instance" "rds" {
  identifier           = local.name
  engine               = var.engine
  engine_version       = var.engine_version
  instance_class       = var.instance_class
  username             = var.master_username
  password             = random_password.rds_root_password.result
  db_subnet_group_name = aws_db_subnet_group.rds_subnet_group.id
  vpc_security_group_ids = compact(
    concat([aws_security_group.rds_default_sg.id], var.additional_security_group_ids)
  )
  multi_az                        = var.multi_az
  parameter_group_name            = var.parameter_group_name
  publicly_accessible             = var.publicly_accessible
  allow_major_version_upgrade     = var.allow_major_version_upgrade
  allocated_storage               = var.storage_size
  storage_type                    = var.storage_type
  storage_encrypted               = var.kms_key_arn != null ? true : var.storage_encrypted
  kms_key_id                      = var.kms_key_arn
  iops                            = var.iops
  snapshot_identifier             = var.snapshot_identifier
  backup_retention_period         = var.backup_retention_period
  monitoring_interval             = var.monitoring_interval
  monitoring_role_arn             = var.monitoring_interval == 0 ? null : aws_iam_role.rds_enhanced_monitoring.arn
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  final_snapshot_identifier       = var.skip_final_snapshot ? null : "${local.name}-final-snapshot"
  skip_final_snapshot             = var.skip_final_snapshot
  deletion_protection             = var.deletion_protection
  apply_immediately               = var.apply_immediately
  copy_tags_to_snapshot           = true
  tags                            = merge(local.common_tags, var.extra_tags)

  lifecycle {
    ignore_changes = [
      engine_version
    ]
  }

}

resource "random_password" "rds_root_password" {
  length  = 32
  upper   = true
  lower   = true
  number  = true
  special = false // RDS cannot contain very special characters
}

resource "aws_db_subnet_group" "rds_subnet_group" {
  name        = "${local.name}-${var.engine}"
  description = "${var.name} ${var.engine} Subnet Group"
  subnet_ids  = var.subnet_ids
  tags        = merge(local.common_tags, var.extra_tags)
}

resource "aws_security_group" "rds_default_sg" {
  name        = "${local.name}-rds-${lower(var.engine)}"
  description = "${var.name} Default RDS Security Group"
  vpc_id      = var.vpc_id
  tags = merge(
    {
      Name = "${var.name} RDS ${var.engine}"
    },
    local.common_tags,
    var.extra_tags
  )
}

//resource "aws_cloudwatch_metric_alarm" "rds_cpu_utilization_high" {
//  alarm_name          = "${var.project_name}_rds_${var.environment}_cpu_utilization_high"
//  alarm_description   = "${var.project_name} RDS ${var.environment} CPUUtilization"
//  comparison_operator = "GreaterThanThreshold"
//  evaluation_periods  = 1
//  metric_name         = "CPUUtilization"
//  namespace           = "AWS/RDS"
//  period              = "300"
//  statistic           = "Average"
//  threshold           = var.alarm_cpu_threshold
//
//  dimensions = {
//    DBInstanceIdentifier = aws_db_instance.rds.id
//  }
//
//  alarm_actions = var.alarm_actions_list
//  tags          = merge(local.common_tags, var.extra_tags)
//}
//
//resource "aws_cloudwatch_metric_alarm" "rds_disk_queue_depth_high" {
//  alarm_name          = "${var.project_name}_rds_${var.environment}_disk_queue_depth_high"
//  alarm_description   = "${var.project_name} ${var.environment} RDS disk queue depth"
//  comparison_operator = "GreaterThanThreshold"
//  evaluation_periods  = "1"
//  metric_name         = "DiskQueueDepth"
//  namespace           = "AWS/RDS"
//  period              = "60"
//  statistic           = "Average"
//  threshold           = var.alarm_disk_queue_threshold
//
//  dimensions = {
//    DBInstanceIdentifier = aws_db_instance.rds.id
//  }
//
//  alarm_actions = var.alarm_actions_list
//  tags          = merge(local.common_tags, var.extra_tags)
//}
//
//resource "aws_cloudwatch_metric_alarm" "rds_disk_free_low" {
//  alarm_name          = "${var.project_name}_rds_${var.environment}-free_storage_space_low"
//  alarm_description   = "${var.project_name} ${var.environment} RDS server free storage space"
//  comparison_operator = "LessThanThreshold"
//  evaluation_periods  = "1"
//  metric_name         = "FreeStorageSpace"
//  namespace           = "AWS/RDS"
//  period              = "60"
//  statistic           = "Average"
//  threshold           = var.alarm_free_disk_threshold
//
//  dimensions = {
//    DBInstanceIdentifier = aws_db_instance.rds.id
//  }
//
//  alarm_actions = var.alarm_actions_list
//  tags          = merge(local.common_tags, var.extra_tags)
//}
