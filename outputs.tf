output "address" {
  value = aws_db_instance.rds.address
}

output "port" {
  value = aws_db_instance.rds.port
}

output "master_username" {
  value     = aws_db_instance.rds.username
  sensitive = true
}

output "master_password" {
  value     = aws_db_instance.rds.password
  sensitive = true
}

output "sg_id" {
  value = aws_security_group.rds_default_sg.id
}
